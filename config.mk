# Copyright (C) 2019 The XEonAX Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

VENDOR_PATH := vendor/xiaomi/ANXCamera

PRODUCT_COPY_FILES += \
        $(VENDOR_PATH)/system/etc/default-permissions/anxcamera-permissions.xml:system/etc/default-permissions/anxcamera-permissions.xml \
	$(VENDOR_PATH)/system/etc/permissions/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	$(VENDOR_PATH)/system/etc/permissions/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	$(VENDOR_PATH)/system/etc/permissions/android.hardware.camera.full.xml:system/etc/permissions/android.hardware.camera.full.xml \
	$(VENDOR_PATH)/system/etc/permissions/android.hardware.camera.raw.xml:system/etc/permissions/android.hardware.camera.raw.xml \
	$(VENDOR_PATH)/system/etc/permissions/privapp-permissions-anxcamera.xml:system/etc/permissions/privapp-permissions-anxcamera.xml \
	$(VENDOR_PATH)/system/etc/sysconfig/anxcamera-hiddenapi-package-whitelist.xml:system/etc/sysconfig/anxcamera-hiddenapi-package-whitelist.xml \
	$(VENDOR_PATH)/system/lib/anxwashere:system/lib/anxwashere \
	$(VENDOR_PATH)/system/lib/libcameraservice.so:system/lib/libcameraservice.so \
	$(VENDOR_PATH)/system/lib64/anxwashere:system/lib64/anxwashere \
	$(VENDOR_PATH)/system/lib64/libcameraservice.so:system/lib64/libcameraservice.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libArcsoft_Avatar_Jni.so:system/priv-app/ANXCamera/lib/arm64/libArcsoft_Avatar_Jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libArcsoft_Avatar_Jni.so:system/priv-app/ANXCamera/lib/arm64/libArcsoft_Avatar_Jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libarcsoft_deflicker.so:system/priv-app/ANXCamera/lib/arm64/libarcsoft_deflicker.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libarcsoft_single_chart_calibration.so:system/priv-app/ANXCamera/lib/arm64/.so:system/priv-app/ANXCamera/lib/arm64/libarcsoft_single_chart_calibration.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libarcsoft_wideselfie.so:system/priv-app/ANXCamera/lib/arm64/libarcsoft_wideselfie.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libarc_layer_sgl.so:system/priv-app/ANXCamera/lib/arm64/libarc_layer_sgl.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libAvatarEngine.so:system/priv-app/ANXCamera/lib/arm64/libAvatarEngine.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libblurbuster.so:system/priv-app/ANXCamera/lib/arm64/libblurbuster.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcamera2ndk.so:system/priv-app/ANXCamera/lib/arm64/libcamera2ndk.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libCameraEffectJNI.so:system/priv-app/ANXCamera/lib/arm64/libCameraEffectJNI.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcamera_960_mpbase.so:system/priv-app/ANXCamera/lib/arm64/libcamera_960_mpbase.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcamera_arcsoft_handgesture.so:system/priv-app/ANXCamera/lib/arm64/libcamera_arcsoft_handgesture.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcamera_handgesture_mpbase.so:system/priv-app/ANXCamera/lib/arm64/libcamera_handgesture_mpbase.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcamera_wideselfie_mpbase.so:system/priv-app/ANXCamera/lib/arm64/libcamera_wideselfie_mpbase.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libchromaflash.so:system/priv-app/ANXCamera/lib/arm64/libchromaflash.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libclearsight.so:system/priv-app/ANXCamera/lib/arm64/libclearsight.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcom.xiaomi.camera.algojni.so:system/priv-app/ANXCamera/lib/arm64/libcom.xiaomi.camera.algojni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libcom.xiaomi.camera.mianodejni.so:system/priv-app/ANXCamera/lib/arm64/libcom.xiaomi.camera.mianodejni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libdeflicker_jni.so:system/priv-app/ANXCamera/lib/arm64/libdeflicker_jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libdualcameraddm.so:system/priv-app/ANXCamera/lib/arm64/libdualcameraddm.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libfacial_action.so:system/priv-app/ANXCamera/lib/arm64/libfacial_action.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libfiltergenerator.so:system/priv-app/ANXCamera/lib/arm64/libfiltergenerator.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libfilterpack_imageproc.so:system/priv-app/ANXCamera/lib/arm64/libfilterpack_imageproc.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_dualcam_refocus.so:system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_dualcam_refocus.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_portrait_lighting.so:system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_portrait_lighting.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_portrait_lighting_c.so:system/priv-app/ANXCamera/lib/arm64/libgallery_arcsoft_portrait_lighting_c.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libgallery_mpbase.so:system/priv-app/ANXCamera/lib/arm64/libgallery_mpbase.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libhandengine.arcsoft.so:system/priv-app/ANXCamera/lib/arm64/libhandengine.arcsoft.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libhazebuster.so:system/priv-app/ANXCamera/lib/arm64/libhazebuster.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libImageSearchAPI.so:system/priv-app/ANXCamera/lib/arm64/libImageSearchAPI.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libimg_utils.so:system/priv-app/ANXCamera/lib/arm64/libimg_utils.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libimscamera_jni.so:system/priv-app/ANXCamera/lib/arm64/libimscamera_jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libimsmedia_jni.so:system/priv-app/ANXCamera/lib/arm64/libimsmedia_jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjnigraphics.so:system/priv-app/ANXCamera/lib/arm64/libjnigraphics.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_blurbuster.so:system/priv-app/ANXCamera/lib/arm64/libjni_blurbuster.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_chromaflash.so:system/priv-app/ANXCamera/lib/arm64/libjni_chromaflash.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_clearsight.so:system/priv-app/ANXCamera/lib/arm64/libjni_clearsight.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_dualcamera.so:system/priv-app/ANXCamera/lib/arm64/libjni_dualcamera.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_filtergenerator.so:system/priv-app/ANXCamera/lib/arm64/libjni_filtergenerator.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_hazebuster.so:system/priv-app/ANXCamera/lib/arm64/libjni_hazebuster.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_load_serinum.so:system/priv-app/ANXCamera/lib/arm64/libjni_load_serinum.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_optizoom.so:system/priv-app/ANXCamera/lib/arm64/libjni_optizoom.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_pacprocessor.so:system/priv-app/ANXCamera/lib/arm64/libjni_pacprocessor.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_resource_drm.so:system/priv-app/ANXCamera/lib/arm64/libjni_resource_drm.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_seestraight.so:system/priv-app/ANXCamera/lib/arm64/libjni_seestraight.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_sharpshooter.so:system/priv-app/ANXCamera/lib/arm64/libjni_sharpshooter.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_stfaceunlock_api.so:system/priv-app/ANXCamera/lib/arm64/libjni_stfaceunlock_api.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_stillmore.so:system/priv-app/ANXCamera/lib/arm64/libjni_stillmore.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_trueportrait.so:system/priv-app/ANXCamera/lib/arm64/libjni_trueportrait.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_truescanner_v2.so:system/priv-app/ANXCamera/lib/arm64/libjni_truescanner_v2.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_ubifocus.so:system/priv-app/ANXCamera/lib/arm64/libjni_ubifocus.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libjni_wideselfie.so:system/priv-app/ANXCamera/lib/arm64/libjni_wideselfie.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmialgoengine.so:system/priv-app/ANXCamera/lib/arm64/libmialgoengine.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmialgo_fs.so:system/priv-app/ANXCamera/lib/arm64/libmialgo_fs.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmialgo_sd.so:system/priv-app/ANXCamera/lib/arm64/libmialgo_sd.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmibokeh_855.so:system/priv-app/ANXCamera/lib/arm64/libmibokeh_855.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmiuinative.so:system/priv-app/ANXCamera/lib/arm64/libmiuinative.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmorpho_memory_allocator.so:system/priv-app/ANXCamera/lib/arm64/libmorpho_memory_allocator.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmorpho_panorama_gp3.so:system/priv-app/ANXCamera/lib/arm64/libmorpho_panorama_gp3.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmorpho_sensor_fusion.so:system/priv-app/ANXCamera/lib/arm64/libmorpho_sensor_fusion.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmotion_photo.so:system/priv-app/ANXCamera/lib/arm64/libmotion_photo.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmotion_photo_c++_shared.so:system/priv-app/ANXCamera/lib/arm64/libmotion_photo_c++_shared.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libmotion_photo_mace.so:system/priv-app/ANXCamera/lib/arm64/libmotion_photo_mace.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libneuralnetworks.so:system/priv-app/ANXCamera/lib/arm64/libneuralnetworks.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/liboptizoom.so:system/priv-app/ANXCamera/lib/arm64/liboptizoom.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libpac.so:system/priv-app/ANXCamera/lib/arm64/libpac.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libqvrcamera_client.qti.so:system/priv-app/ANXCamera/lib/arm64/libqvrcamera_client.qti.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/librefocus.so:system/priv-app/ANXCamera/lib/arm64/librefocus.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/librefocus_mibokeh.so:system/priv-app/ANXCamera/lib/arm64/librefocus_mibokeh.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libseccam.so:system/priv-app/ANXCamera/lib/arm64/libseccam.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libseestraight.so:system/priv-app/ANXCamera/lib/arm64/libseestraight.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libshootdetect.so:system/priv-app/ANXCamera/lib/arm64/libshootdetect.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtriplecam_optical_zoom_control.so:system/priv-app/ANXCamera/lib/arm64/libtriplecam_optical_zoom_control.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtriplecam_video_optical_zoom.so:system/priv-app/ANXCamera/lib/arm64/libtriplecam_video_optical_zoom.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtrueportrait.so:system/priv-app/ANXCamera/lib/arm64/libtrueportrait.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtruescanner.so:system/priv-app/ANXCamera/lib/arm64/libtruescanner.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libttaudioeffect.so:system/priv-app/ANXCamera/lib/arm64/libttaudioeffect.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libttffmpeg-invoker.so:system/priv-app/ANXCamera/lib/arm64/libttffmpeg-invoker.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libttffmpeg.so:system/priv-app/ANXCamera/lib/arm64/libttffmpeg.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libttvideoeditor.so:system/priv-app/ANXCamera/lib/arm64/libttvideoeditor.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libttyuv.so:system/priv-app/ANXCamera/lib/arm64/libttyuv.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtt_effect.so:system/priv-app/ANXCamera/lib/arm64/libtt_effect.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtt_jni.so:system/priv-app/ANXCamera/lib/arm64/libtt_jni.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libtzcom.so:system/priv-app/ANXCamera/lib/arm64/libtzcom.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libubifocus.so:system/priv-app/ANXCamera/lib/arm64/libubifocus.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libVDClearShot.so:system/priv-app/ANXCamera/lib/arm64/libVDClearShot.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libvideo_extra_color_converter.so:system/priv-app/ANXCamera/lib/arm64/libvideo_extra_color_converter.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libvideo_extra_interpolator.so:system/priv-app/ANXCamera/lib/arm64/libvideo_extra_interpolator.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libvidhance.so:system/priv-app/ANXCamera/lib/arm64/libvidhance.so \
	$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64/libvndksupport.so:system/priv-app/ANXCamera/lib/arm64/libvndksupport.so

PRODUCT_PACKAGES += \
    ANXCamera
